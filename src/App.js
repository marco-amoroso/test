import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { Provider } from 'react-redux'
import Products from './containers/Products'
import store from './store'

const NotFound = () => <span>Not found</span>

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={Products} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  </Provider>
)

export default App
