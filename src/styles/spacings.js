export default {
  SMALL: '5px',
  MEDIUM: '10px',
  LARGE: '20px',
}
