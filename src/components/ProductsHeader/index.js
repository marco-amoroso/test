// @flow
import React from 'react'
import styled from 'styled-components'
import colors from '../../styles/colors'
import spacings from '../../styles/spacings'

const SELECT_OPTIONS = ['8', '12', '16']

const Header = styled.header`
  border-bottom: 1px solid ${colors.BORDER};
  padding-bottom: ${spacings.SMALL};
  margin-bottom: ${spacings.MEDIUM};
  display: grid;
  grid-gap: ${spacings.MEDIUM};
  grid-template-areas: 
    "header-title header-title"
    "header-subtitle header-select";
  grid-template-columns: auto 100px;
`

const Title = styled.div`
  color: ${colors.HEADING};
  font-weight: 600;
  font-size: 1.5em;
  grid-area: header-title;
`

const SubTitle = styled.div`
  font-size: 0.8em;
  color: ${colors.HEADING};
  grid-area: header-subtitle;
  align-items: center;
  display: flex;
`

const Select = styled.select`
  grid-area: header-select;
`

type Props = {
  total: number,
  onSizeChange: () => {},
}

const ProductsHeader = ({ total, onSizeChange }: Props) => (
  <Header>
    <Title>All Products</Title>
    <SubTitle>{total} Products</SubTitle>
    <Select onChange={onSizeChange}>
      {SELECT_OPTIONS.map(option => (
        <option key={option} value={option}>{option} per page</option>
      ))}
    </Select>
  </Header>
)

export default ProductsHeader
