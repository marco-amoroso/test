// @flow
import React from 'react'
import styled from 'styled-components'
import Product from '../Product'
import type { ProductType } from '../Product'
import spacings from '../../styles/spacings'

const Wrapper = styled.div`
  padding-bottom: ${spacings.LARGE};
  display: grid;
  grid-gap: ${spacings.MEDIUM};
  grid-template-columns: repeat(auto-fill,minmax(250px, 1fr));
`
export type ProductsType = {
  products: Array<ProductType>,
}

const ProductListings = ({ products }: ProductsType) => (
  <Wrapper>
    {Array.isArray(products)
      ? products.map(product => <Product key={product.id} {...product} />)
      : 'No products'
    }
  </Wrapper>
)

export default ProductListings
