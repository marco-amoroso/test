import React from 'react'
import Product from '../Product'
import renderer from 'react-test-renderer'
import mockProducts from '../../utils/mock'

const mockedProduct = mockProducts(1)[0]

describe('<Product />', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<Product {...mockedProduct} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders correctly with missing props', () => {
    const tree = renderer
      .create(<Product />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
