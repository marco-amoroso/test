// @flow
import React from 'react'
import styled from 'styled-components'
import colors from '../../styles/colors'
import spacings from '../../styles/spacings'

const Wrapper = styled.div`
  border: 1px solid ${colors.BORDER};
  display: flex;
  flex-direction: column;
  height: 100%;
  background-color: ${colors.WHITE};
`

const Image = styled.img`
  object-fit: contain;
  padding: ${spacings.MEDIUM};
  max-height: 180px;
`

const Info = styled.div`
  padding: ${spacings.MEDIUM};
  border-top: 1px solid ${colors.BORDER};
`

const Name = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: ${colors.HEADING};
  font-weight: 600;
  margin-bottom: ${spacings.SMALL};
`

const Description = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: ${colors.HEADING};
  margin-bottom: ${spacings.SMALL};
`

const Price = styled.div`
  font-weight: bold;
`

export type ProductType = {
  id: number,
  price: string,
  product_name: string,
  description: string,
  product_image: string,
}

const Product = ({
  id,
  price,
  product_name,
  description,
  product_image
}: ProductType) => (
  <Wrapper key={id}>
    <Image src={product_image || 'https://dummyimage.com/307x328.bmp/ff4444/ffffff'} alt={product_name} />
    <Info>
      <Name>{product_name || 'No product name'}</Name>
      <Description>{description || 'No description'}</Description>
      <Price>{price || 'No price'}</Price>
    </Info>
  </Wrapper>
)

export default Product
