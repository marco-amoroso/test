import React from 'react'
import { shallow, configure } from 'enzyme'
import Pagination, { Button, PrevButton, NextButton } from '../Pagination'
import renderer from 'react-test-renderer'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() })

describe('<Pagination />', () => {
  it('renders correctly', () => {
    const defaultProps = {
      currentPage: 1,
      totalPages: 10,
      onPageClick: () => {},
    }
    const tree = renderer
      .create(<Pagination {...defaultProps} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders correctly with missing props', () => {
    const tree = renderer
      .create(<Pagination />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})

describe('<Pagination /> at page 1 out of 10 pages', () => {
  let wrapper
  let mockCallBack
  beforeEach(() => {
    mockCallBack = jest.fn()
    const defaultProps = {
      currentPage: 1,
      totalPages: 10,
      onPageClick: mockCallBack,
    }
    wrapper = shallow(<Pagination {...defaultProps} />)
  })
  it('renders correctly 3 <Button>s', () => {
    expect(wrapper.find(Button)).toHaveLength(3)    
  })
  
  it('handles correctly the first <Button>', () => {
    const FirstButton = wrapper.find(Button).at(0)
    FirstButton.simulate('click')

    expect(FirstButton).toHaveLength(1)
    expect(mockCallBack).not.toHaveBeenCalled()
  })
  it('handles correctly the second <Button>', () => {
    const FirstButton = wrapper.find(Button).at(1)
    FirstButton.simulate('click')

    expect(FirstButton).toHaveLength(1)
    expect(mockCallBack).toHaveBeenCalledWith(2)
  })
  it('handles correctly the third <Button>', () => {
    const FirstButton = wrapper.find(Button).at(2)
    FirstButton.simulate('click')

    expect(FirstButton).toHaveLength(1)
    expect(mockCallBack).toHaveBeenCalledWith(3)
  })
  it('renders correctly 1 <PrevButton>', () => {
    const Prev = wrapper.find(PrevButton)
    Prev.simulate('click')

    expect(Prev).toHaveLength(1)
    expect(mockCallBack).not.toHaveBeenCalled()
  })
  it('renders correctly 1 <NextButton>', () => {
    const Next = wrapper.find(NextButton)
    Next.simulate('click')

    expect(Next).toHaveLength(1)
    expect(mockCallBack).toHaveBeenCalledWith(2)
  })
})

describe('<Pagination /> at page 9 out of 10 pages', () => {
  let wrapper
  let mockCallBack
  beforeEach(() => {
    mockCallBack = jest.fn()
    const defaultProps = {
      currentPage: 9,
      totalPages: 10,
      onPageClick: mockCallBack,
    }
    wrapper = shallow(<Pagination {...defaultProps} />)
  })
  it('renders correctly 2 <Button>s', () => {
    expect(wrapper.find(Button)).toHaveLength(2)
  })
  
  it('handles correctly the first <Button>', () => {
    const FirstButton = wrapper.find(Button).at(0)
    FirstButton.simulate('click')

    expect(FirstButton).toHaveLength(1)
    expect(mockCallBack).not.toHaveBeenCalled()
  })
  it('handles correctly the second <Button>', () => {
    const FirstButton = wrapper.find(Button).at(1)
    FirstButton.simulate('click')

    expect(FirstButton).toHaveLength(1)
    expect(mockCallBack).toHaveBeenCalledWith(10)
  })
  it('renders correctly 1 <PrevButton>', () => {
    const Prev = wrapper.find(PrevButton)
    Prev.simulate('click')

    expect(Prev).toHaveLength(1)
    expect(mockCallBack).toHaveBeenCalledWith(8)
  })
  it('renders correctly 1 <NextButton>', () => {
    const Next = wrapper.find(NextButton)
    Next.simulate('click')

    expect(Next).toHaveLength(1)
    expect(mockCallBack).toHaveBeenCalledWith(10)
  })
})

describe('<Pagination /> at page 10 out of 10 pages', () => {
  let wrapper
  let mockCallBack
  beforeEach(() => {
    mockCallBack = jest.fn()
    const defaultProps = {
      currentPage: 10,
      totalPages: 10,
      onPageClick: mockCallBack,
    }
    wrapper = shallow(<Pagination {...defaultProps} />)
  })
  it('renders correctly 1 <Button>', () => {
    expect(wrapper.find(Button)).toHaveLength(1)    
  })
  
  it('handles correctly the first <Button>', () => {
    const FirstButton = wrapper.find(Button).at(0)
    FirstButton.simulate('click')

    expect(FirstButton).toHaveLength(1)
    expect(mockCallBack).not.toHaveBeenCalled()
  })
  it('renders correctly 1 <PrevButton>', () => {
    const Prev = wrapper.find(PrevButton)
    Prev.simulate('click')

    expect(Prev).toHaveLength(1)
    expect(mockCallBack).toHaveBeenCalledWith(9)
  })
  it('renders correctly 1 <NextButton>', () => {
    const Next = wrapper.find(NextButton)
    Next.simulate('click')

    expect(Next).toHaveLength(1)
    expect(mockCallBack).not.toHaveBeenCalled()
  })
})
