// @flow
import React from 'react'
import styled from 'styled-components'
import colors from '../../styles/colors'
import spacings from '../../styles/spacings'

type Props = {
  currentPage: number,
  totalPages: number,
  onPageClick: (number) => {},
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;

  @media (min-width: 768px) {
    justify-content: flex-end;  
  }
`

export const Button = styled.span`
  display: flex;
  text-align: center;
  min-width: 5px;
  padding: ${spacings.SMALL} ${spacings.MEDIUM} ${spacings.SMALL};
  align-items: center;
  cursor: ${props => !props.disabled && 'pointer'};
  background-color: ${props => props.disabled && colors.WHITE};
  border-bottom: 3px solid ${props => props.disabled ? colors.DARK_BORDER : 'transparent'};
  user-select: none;
`

const PrevNextButton = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: ${props => props.disabled && '0.3'};
  cursor: ${props => !props.disabled && 'pointer'};
  padding: ${spacings.SMALL} ${spacings.MEDIUM} ${spacings.SMALL};
  user-select: none;
  border-bottom: 3px solid transparent;
`

export const PrevButton = styled(PrevNextButton)`
  &:before {
    content: '<';
    display: inline-block;
    margin-right: ${spacings.MEDIUM};
  }
`

export const NextButton = styled(PrevNextButton)`
  text-align: right;
    
  &:after {
    content: '>';
    display: inline-block;
    margin-left: ${spacings.MEDIUM};
  }
`

const Pagination = ({
  currentPage,
  totalPages,
  onPageClick,
}: Props) => {
  const isPrevDisabled = !(currentPage > 1)
  const isNextDisabled = !(currentPage < totalPages)

  // TODO(marco): Handle fallback with UI error
  if (Number.isNaN(currentPage) || !currentPage || Number.isNaN(totalPages) || !totalPages) return null

  return (
    <Wrapper>
      <PrevButton
        onClick={() => !isPrevDisabled && onPageClick(currentPage - 1)}
        disabled={isPrevDisabled}
      >
        Previous page
      </PrevButton>
      {[
        currentPage - 1,
        currentPage < totalPages ? currentPage : -1,
        currentPage + 1 < totalPages ? currentPage + 1 : -1
      ].map(page => {
        if (page < 0) return null
        const isCurrent = currentPage === page + 1
        return (
          <Button
            key={`button-page-${page + 1}`}
            onClick={() => !isCurrent && onPageClick(page + 1)}
            disabled={isCurrent}
          >
            {page + 1}
          </Button>
        )
      })}
      <NextButton
        onClick={() => !isNextDisabled && onPageClick(currentPage + 1)}
        disabled={isNextDisabled}
      >
        Next page
      </NextButton>
    </Wrapper>
  )
}

export default Pagination
