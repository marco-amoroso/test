// @flow

import { combineReducers } from 'redux'
import productsReducer from '../containers/Products/reducer'

import {createStore, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

const reducers = combineReducers({
  products: productsReducer,
})

// Redux DevTools
// https://github.com/zalmoxisus/redux-devtools-extension#12-advanced-store-setup
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  reducers,
  composeEnhancers(
    applyMiddleware(thunk),
  ),
)

export default store
