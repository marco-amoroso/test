// @flow
import { FETCH_PRODUCTS_LOADING, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR, SELECT_PAGE, SELECT_SIZE } from './constants'
import type { ProductsType } from '../../components/ProductListings'

type State = {
  products: Array<ProductsType>,
  loading: boolean,
  currentPage: number,
  size: number,
  totalPages: number,
}

type Action = {
  type: string,
  payload: any,
}

export const initialState = {
  products: [],
  loading: false,
  currentPage: 1,
  size: 8,
  totalPages: 1,
}

export default function productsReducer(state: State = initialState, action: Action) {
  switch (action.type) {
    case FETCH_PRODUCTS_LOADING:
      return {
        ...state,
        loading: true,
      }
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        products: action.payload.products,
        totalPages: Math.ceil(action.payload.products.length / state.size),
      }
    case FETCH_PRODUCTS_ERROR:
      return {
        ...state,
        loading: false,
      }
    case SELECT_PAGE: {
      return {
        ...state,
        currentPage: action.payload.currentPage,
      }
    }
    case SELECT_SIZE: {
      return {
        ...state,
        currentPage: 1,
        size: action.payload.size,
        totalPages: Math.ceil(state.products.length / action.payload.size),
      }
    }
    default:
      return state
  }
}
