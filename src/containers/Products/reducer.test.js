import reducer, { initialState } from './reducer'
import { FETCH_PRODUCTS_LOADING, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR, SELECT_PAGE, SELECT_SIZE } from './constants'
import mockProducts from '../../utils/mock'

const mockedProducts = mockProducts(10)

const stateWithTenProducts = {
  ...initialState,
  products: mockedProducts,
  totalPages: 2,
  size: 8,
}

describe('products reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  it('should handle FETCH_PRODUCTS_LOADING', () => {
    expect(
      reducer(initialState, {
        type: FETCH_PRODUCTS_LOADING,
      })
    ).toEqual(
      {
        ...initialState,
        loading: true,
      }
    )
  })

  it('should handle FETCH_PRODUCTS_SUCCESS', () => {
    expect(
      reducer(initialState, {
        type: FETCH_PRODUCTS_SUCCESS,
        payload: {
          products: mockedProducts,
        }
      })
    ).toEqual(stateWithTenProducts)
  })

  it('should handle FETCH_PRODUCTS_ERROR', () => {
    expect(
      reducer(initialState, {
        type: FETCH_PRODUCTS_ERROR,
      })
    ).toEqual(initialState)
  })

  it('should handle SELECT_PAGE', () => {
    expect(
      reducer(initialState, {
        type: SELECT_PAGE,
        payload: {
          currentPage: 2
        }
      })
    ).toEqual({
      ...initialState,
      currentPage: 2
    })
  })

  it('should handle SELECT_SIZE', () => {
    expect(
      reducer(stateWithTenProducts, {
        type: SELECT_SIZE,
        payload: {
          size: 4
        }
      })
    ).toEqual({
      ...stateWithTenProducts,
      size: 4,
      totalPages: 3,
    })
  })
})
