// @flow
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { branch, compose, lifecycle, renderComponent } from 'recompose'
import { selectPage, selectSize, fetchProductsLoading, fetchProductsSuccess, fetchProductsError } from './actions'

const mapStateToProps = ({ products }) => ({
  products: products.products,
  loading: products.loading,
  currentPage: products.currentPage,
  size: products.size,
  totalPages: products.totalPages,
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    onSizeChange: (event) => selectSize(event.target.value),
    onPageClick: (page) => selectPage(page),
    onFetchLoading: fetchProductsLoading,
    onFetchSuccess: fetchProductsSuccess,
    onFetchError: fetchProductsError,
  }, dispatch)
}

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  lifecycle({
    async componentDidMount() {
      const { onFetchLoading, onFetchSuccess, onFetchError } = this.props

      // TODO(marco): Implement loading UI
      onFetchLoading()

      try {
        const response = await fetch('https://whitechdevs.github.io/reactjs-test/products.json')
        const products = await response.json()
        onFetchSuccess(products)
      } catch(err) {
        // TODO(marco): Implement error UI
        onFetchError()
      }
    }
  }),
  branch(({ products }) => !Array.isArray(products), renderComponent(() => <span>No products</span>))
)



export default enhance
