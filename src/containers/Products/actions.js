// @flow
import { SELECT_PAGE, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_LOADING, SELECT_SIZE } from './constants'
import type { ProductsType } from '../../components/ProductListings'

const fetchProductsLoading = () => ({
  type: FETCH_PRODUCTS_LOADING,
})

const fetchProductsSuccess = (products: ProductsType) => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: {
    products,
  }
})

const fetchProductsError = () => ({
  type: FETCH_PRODUCTS_ERROR,
})

const selectSize = (size: number) => ({
  type: SELECT_SIZE,
  payload: {
    size: parseInt(size),
  },
})

const selectPage = (selectedPage: number) => ({
  type: SELECT_PAGE,
  payload: {
    currentPage: parseInt(selectedPage),
  },
})

export { selectPage, selectSize, fetchProductsLoading, fetchProductsSuccess, fetchProductsError }
